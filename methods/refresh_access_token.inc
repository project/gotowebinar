<?php
/**
 * GoToWebinar OAuth method; Get Access Token.
 */
$method = array(
  'url' => url(GOTOWEBINAR_OAUTH_URL . 'token'),
  'data' => http_build_query(array(
    'grant_type' => 'refresh_token',
    'refresh_token' => $settings['refresh_token'],
  )),
  'headers' => array(
    'Authorization' => 'Basic ' . base64_encode("{$settings['api_key']}:{$settings['api_secret']}"),
    'Content-Type' => 'application/x-www-form-urlencoded',
  ),
  'method' => 'POST',
  'response_type' => 'json',
);
