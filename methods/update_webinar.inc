<?php
/**
 * GoToWebinar API method; Update webinar.
 *
 * TODO Not implemented, see https://developer.citrixonline.com/content/gotowebinar-api-reference
 */

$method = array(
  'url' => url(GOTOWEBINAR_API_URL . "{$settings['organizer_key']}/webinars/{$options['params']['webinar_key']}", array(
    'query' => array(
      'oauth_token' => $settings['access_token'],
    ),
  )),
  'method' => 'POST',
  'response_type' => 'json',
);
