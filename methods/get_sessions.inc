<?php
/**
 * GoToWebinar API method; Get Sessions.
 *
 */

$method = array(
  'url' => url(GOTOWEBINAR_API_URL . "{$settings['organizer_key']}/webinars/{$options['params']['webinar_key']}/sessions", array(
    'query' => array(
      'oauth_token' => $settings['access_token'],
    ),
  )),
  'method' => 'GET',
  'response_type' => 'json',
);
