<?php
/**
 * GoToWebinar API method; Get Registrants.
 *
 */

$method = array(
  'url' => url(GOTOWEBINAR_API_URL . "{$settings['organizer_key']}/webinars/{$options['params']['webinar_key']}/registrants", array(
    'query' => array(
      'oauth_token' => $settings['access_token'],
    ),
  )),
  'method' => 'GET',
  'response_type' => 'json',
);
