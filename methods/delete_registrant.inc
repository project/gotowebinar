<?php
/**
 * GoToWebinar API method; Delete Registrant.
 *
 */

$method = array(
  'url' => url(GOTOWEBINAR_API_URL . "{$settings['organizer_key']}/webinars/{$options['params']['webinar_key']}/registrants/{$options['params']['registrant_key']}", array(
    'query' => array(
      'oauth_token' => $settings['access_token'],
    ),
  )),
  'method' => 'DELETE',
  'response_type' => 'json',
);
