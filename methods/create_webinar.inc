<?php
/**
 * GoToWebinar API method; Create Webinar.
 *
 */

$method = array(
  'url' => url(GOTOWEBINAR_API_URL . "{$settings['organizer_key']}/webinars", array(
    'query' => array(
      'oauth_token' => $settings['access_token'],
    ),
  )),
  'method' => 'POST',
  'response_type' => 'json',
);
