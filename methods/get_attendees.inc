<?php
/**
 * GoToWebinar API method; Get Attendees.
 *
 */

$method = array(
  'url' => url(GOTOWEBINAR_API_URL . "{$settings['organizer_key']}/webinars/{$options['params']['webinar_key']}/sessions/{$options['params']['session_key']}/attendees", array(
    'query' => array(
      'oauth_token' => $settings['access_token'],
    ),
  )),
  'method' => 'GET',
  'response_type' => 'json',
);
